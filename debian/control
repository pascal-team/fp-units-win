Source: fp-units-win
Section: devel
Priority: optional
Maintainer: Pascal Packaging Team <pkg-pascal-devel@lists.alioth.debian.org>
Uploaders:
 Abou Al Montacir <abou.almontacir@sfr.fr>,
 Paul Gevers <elbrus@debian.org>,
 Peter Michael Green <plugwash@debian.org>,
Standards-Version: 4.7.0
Build-Depends:
 debhelper (>= 13),
 dh-exec (>=0.22),
 fp-compiler,
 fp-units-base,
 fp-units-fcl,
 fp-utils,
 fpc-source,
Vcs-Git: https://salsa.debian.org/pascal-team/fp-units-win.git
Vcs-Browser: https://salsa.debian.org/pascal-team/fp-units-win
Homepage: https://www.freepascal.org/

Package: fp-units-win-rtl-3.2.2
Architecture: amd64 i386
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 fp-compiler-3.2.2 (>= 3.2.2+dfsg-46)
Provides:
 fp-units-win-rtl,
 fpc-abi-3.2.2
Breaks:
 fpc (<= 3.2.2+dfsg-0),
 fp-compiler-3.2.2 (<= 3.2.2+dfsg-10)
Replaces:
 fpc (<= 3.2.2+dfsg-0),
 fp-compiler-3.2.2 (<= 3.2.2+dfsg-10)
Description: Free Pascal - runtime libraries
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains the RunTime Libraries for the Free Pascal Compiler.

Package: fp-units-win-base-3.2.2
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-rtl-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends},
 ${shlibs:Depends}
Suggests:
 uuid-dev
Provides:
 fp-units-win-base
Breaks:
 fpc (<= 3.2.2+dfsg-0),
Replaces:
 fpc (<= 3.2.2+dfsg-0),
Description: Free Pascal - base units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains Free Pascal units for common libraries (some of which
 are also required by the Free Component Library): NCurses, X11 (Xlib,
 Xutil), and ZLib.

Package: fp-units-win-fcl-3.2.2
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-base-3.2.2 (>= 3.2.2+dfsg-46),
 fp-units-win-rtl-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 fp-units-win-fcl
Breaks:
 fpc (<= 3.2.2+dfsg-0),
Replaces:
 fpc (<= 3.2.2+dfsg-0),
Description: Free Pascal - Free Component Library
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains the Free Component Library for the Free Pascal Compiler.

Package: fp-units-win-fv-3.2.2
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-rtl-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 fp-units-win-fv
Breaks:
 fpc (<= 3.2.2+dfsg-0),
Replaces:
 fpc (<= 3.2.2+dfsg-0),
Description: Free Pascal - Free Vision units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains the Free Vision units for the Free Pascal Compiler
 (which provide a framework for developing text user interfaces).

Package: fp-units-win-gtk2-3.2.2
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-fcl-3.2.2 (>= 3.2.2+dfsg-46),
 fp-units-win-rtl-3.2.2 (>= 3.2.2+dfsg-46),
 libgtk2.0-dev,
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 fp-units-win-gtk2
Breaks:
 fpc (<= 3.2.2+dfsg-0),
Replaces:
 fpc (<= 3.2.2+dfsg-0),
Description: Free Pascal - GTK+ 2.x units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains Free Pascal units and examples to create
 programs with GTK+ 2.x.

Package: fp-units-win-db-3.2.2
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-rtl-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends},
 ${shlibs:Depends}
Suggests:
 firebird-dev,
 freetds-dev,
 libgdbm-dev,
 default-libmysqlclient-dev,
 libpq-dev,
 libsqlite3-dev,
 pxlib-dev,
 unixodbc-dev
Provides:
 fp-units-win-db
Breaks:
 fpc (<= 3.2.2+dfsg-0),
Replaces:
 fpc (<= 3.2.2+dfsg-0),
Description: Free Pascal - database-library units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains Free Pascal units with bindings for GDBM, Interbase,
 MySQL, PostgreSQL, ODBC, Oracle, and SQLite.

Package: fp-units-win-gfx-3.2.2
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-base-3.2.2 (>= 3.2.2+dfsg-46),
 fp-units-win-rtl-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 fp-units-win-gfx
Recommends:
 libcairo2-dev,
 libforms-dev,
 libgd-dev,
 libgl-dev,
 libgraphviz-dev,
 libpng-dev,
 libxxf86dga-dev,
 libxxf86vm-dev
Breaks:
 fpc (<= 3.2.2+dfsg-0),
Replaces:
 fpc (<= 3.2.2+dfsg-0),
Description: Free Pascal - graphics-library units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains Free Pascal units with bindings for cairo, forms, ggi,
 graph, libgd, libpng, opengl, and svgalib.
 .
 SVGALib is no longer packaged by Debian and should be installed manually by
 users who want to link against it.

Package: fp-units-win-net-3.2.2
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-rtl-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 fp-units-win-net
Breaks:
 fpc (<= 3.2.2+dfsg-0),
Replaces:
 fpc (<= 3.2.2+dfsg-0),
Description: Free Pascal - networking units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains Free Pascal units for creating network tools: D-Bus,
 httpd-1.3, httpd-2.0, httpd-2.2, ldap, libasync, libcurl, netdb, openssl,
 and pcap.

Package: fp-units-win-math-3.2.2
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-rtl-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 libgmp-dev
Provides:
 fp-units-win-math
Breaks:
 fpc (<= 3.2.2+dfsg-0),
Replaces:
 fpc (<= 3.2.2+dfsg-0),
Description: Free Pascal - math units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains Free Pascal math interfacing units for:
  * gmp (the GNU Multiple Precision arithmetic library);
  * numlib (numerical computing);
  * proj4 (cartographic projections);
  * symbolic (symbolic computing).

#note: when backporting to stretch or earlier disable ncurses6.patch
#and then change the libncursesw5-dev breaks to >= 6.1+20180210
Package: fp-units-win-misc-3.2.2
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-rtl-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 fp-units-win-misc
Breaks:
 fpc (<= 3.2.2+dfsg-0),
 libncursesw5-dev (<< 6.1+20180210)
Replaces:
 fpc (<= 3.2.2+dfsg-0),
Description: Free Pascal - miscellaneous units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains miscellaneous Free Pascal units: fppkg (the FPC
 packaging system), PasZLib (a Pascal-only zlib implementation), and Utmp.

Package: fp-units-win-multimedia-3.2.2
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-rtl-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 a52dec-dev,
 libdts-dev,
 libmad0-dev,
 libmodplug-dev,
 libogg-dev,
 libsdl-mixer1.2-dev,
 libvorbis-dev,
 libvlc-dev
Provides:
 fp-units-win-multimedia
Breaks:
 fpc (<= 3.2.2+dfsg-0),
Replaces:
 fpc (<= 3.2.2+dfsg-0),
Description: Free Pascal - multimedia units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains Free Pascal multimedia units: a52, dts, mad, modplug,
 oggvorbis, openal, and vlc.

Package: fp-units-win-wasm-3.2.2
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-rtl-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends},
 ${shlibs:Depends}
Provides:
 fp-units-win-wasm
Breaks:
 fpc (<= 3.2.2+dfsg-0),
Replaces:
 fpc (<= 3.2.2+dfsg-0),
Description: Free Pascal - Web assembly support units
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This package contains Free Pascal web assembly support units: pas2js,
 utils-pas2js, webidl ...

Package: fp-units-win-rtl
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-rtl-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends}
Description: Free Pascal - runtime libraries dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing the RunTime Libraries for the Free Pascal Compiler.

Package: fp-units-win-base
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-base-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends}
Description: Free Pascal - base units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing Free Pascal units for common libraries (some of which
 are also required by the Free Component Library): NCurses, X11 (Xlib,
 Xutil), and ZLib.

Package: fp-units-win-fcl
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-fcl-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends}
Description: Free Pascal - Free Component Library dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing the Free Component Library for the Free Pascal Compiler.

Package: fp-units-win-fv
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-fv-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends}
Description: Free Pascal - Free Vision units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing the Free Vision units for the Free Pascal Compiler
 (which provide a framework for developing text user interfaces).

Package: fp-units-win-gtk2
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-gtk2-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends}
Description: Free Pascal - GTK+ 2.x units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing Free Pascal units and examples to create
 programs with GTK+ 2.x.

Package: fp-units-win-db
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-db-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends}
Description: Free Pascal - database-library units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing Free Pascal units with bindings for GDBM, Interbase,
 MySQL, PostgreSQL, ODBC, Oracle, and SQLite.

Package: fp-units-win-gfx
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-gfx-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends}
Description: Free Pascal - graphics-library units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing Free Pascal units with bindings for cairo, forms, ggi,
 graph, libgd, libpng, opengl, and svgalib.
 .
 SVGALib is no longer packaged by Debian and should be installed manually by
 users who want to link against it.

Package: fp-units-win-net
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-net-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends}
Description: Free Pascal - networking units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing Free Pascal units for creating network tools: D-Bus,
 httpd-1.3, httpd-2.0, httpd-2.2, ldap, libasync, libcurl, netdb, openssl,
 and pcap.

Package: fp-units-win-math
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-math-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends}
Description: Free Pascal - math units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing Free Pascal math interfacing units for:
  * gmp (the GNU Multiple Precision arithmetic library);
  * numlib (numerical computing);
  * proj4 (cartographic projections);
  * symbolic (symbolic computing).

Package: fp-units-win-misc
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-misc-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends}
Description: Free Pascal - miscellaneous units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing miscellaneous Free Pascal units: fppkg (the FPC
 packaging system), PasZLib (a Pascal-only zlib implementation), and Utmp.

Package: fp-units-win-multimedia
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-multimedia-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends}
Description: Free Pascal - multimedia units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing Free Pascal multimedia units: a52, dts, mad, modplug,
 oggvorbis, openal, and vlc.

Package: fp-units-win-wasm
Architecture: amd64 i386
Multi-Arch: same
Depends:
 fp-units-win-wasm-3.2.2 (>= 3.2.2+dfsg-46),
 ${misc:Depends}
Description: Free Pascal - Web assembly support units dependency package
 The Free Pascal Compiler is an Object Pascal compiler supporting both Delphi
 and Turbo Pascal 7.0 dialects, as well as Mac Pascal dialects. It provides a
 completely portable RunTime Library (RTL) available on many platforms and
 compatible with Turbo Pascal, along with a platform-independent class-based
 Free Component Library (FCL) adding many Delphi extensions and interfacing
 with many popular open source libraries.
 .
 This dependency package always depends on the latest available version of
 the package containing the Free Pascal web assembly support units: pas2js,
 utils-pas2js, webidl ...
